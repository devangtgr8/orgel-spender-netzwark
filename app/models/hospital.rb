class Hospital < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  attr_accessible :email, :password, :hospital_name, :hospital_id, :address, :city, :state, :district, :zip, :phone

  validates_presence_of :hospital_name, :hospital_id, :address, :city, :state, :district, :zip, :phone
end
