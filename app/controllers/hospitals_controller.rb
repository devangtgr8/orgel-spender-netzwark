class HospitalsController < ApplicationController
  def new
  end

  def show
    @blood = Blood.new
  end

  def update
  end

  def edit
  end

  def index
    @hospitals = Hospital.paginate(page: params[:page])
  end
end
