class AddVerifiedToHospitals < ActiveRecord::Migration
  def change
    add_column :hospitals, :verify, :boolean, default: false
  end
end
