class AddFieldsToHospital < ActiveRecord::Migration
  def change
    add_column :hospitals, :hospital_name, :string, :null => false
    add_column :hospitals, :hospital_id, :integer, :null => false
    add_column :hospitals, :address, :string, :null => false
    add_column :hospitals, :city, :string, :null => false
    add_column :hospitals, :district, :string, :null => false
    add_column :hospitals, :state, :string, :null => false
    add_column :hospitals, :zip, :integer, :null => false
    add_column :hospitals, :phone, :integer, :null => false
  end
end
